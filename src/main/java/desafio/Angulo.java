package desafio;

public class Angulo {
	private String angulo;
	private String time;

	public String getAngulo() {
		return angulo;
	}

	public void setAngulo(String angulo) {
		this.angulo = angulo;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
