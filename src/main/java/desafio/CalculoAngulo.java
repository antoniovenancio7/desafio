package desafio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

@Path("clock")
public class CalculoAngulo {

	@Path("{time}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response calculo(@PathParam("time") String time) {

		String fileName = "angulo.json";
		try {
			if (!Files.exists(Paths.get(fileName))) {
				Files.createFile(Paths.get(fileName));
			}

			Stream<String> stream = Files.lines(Paths.get(fileName));

			String param = "\"" + "time" + "\"" + ":" + "\"" + time + "\"";

			String s = stream.filter(line -> line.contains(param)).findFirst().orElse("");

			Angulo angulo = new Angulo();
			Gson g = new Gson();

			// angulo = mapper.readValue(new File(path), Angulo.class);

			String json = null;
			String[] split = {};
			if (s.trim().length() == 0) {
				int d = calcularAngulo(time);

				angulo.setTime(time);
				angulo.setAngulo(String.valueOf(d));

				json = g.toJson(angulo);
				
				List<String> lstJson = new ArrayList<>();
				lstJson.add(json);
				
				Files.write(Paths.get(fileName), lstJson, StandardOpenOption.APPEND);
				split = json.split(",");
				// json = g.toJson(resultado);

			} else {
				split = s.split(",");
			}

			return Response.status(200).entity(split[0] + "}").build();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(500).entity("algo deu errado").build();
		}

	}

	private int calcularAngulo(String time) {

		int hp = 60;
		int mp = 11;

		String[] s = time.split(":");

		int h = Integer.parseInt(s[0]);
		int m = 0;
		if (s.length > 1) {
			m = Integer.parseInt(s[1]);
		}

		int angulo = ((mp * m) - (hp * h)) / 2;
		angulo *= -1;
		if (angulo > 180) {// torna o numero positivo
			angulo = 360 - angulo;
		}

		return angulo;

	}

}
